#!/bin/sh

set -x

ev() {
  d=libev-4.15
  rm -rf ev
  wget -q http://dist.schmorp.de/libev/Attic/$d.tar.gz\
    && tar -xzf $d.tar.gz\
    && rm -f $d.tar.gz\
    && mv $d ev
}

utf8proc() {
  v=v1.1.5
  rm -rf utf8proc
  wget -q http://www.public-software-group.org/pub/projects/utf8proc/$v/utf8proc-$v.tar.gz\
    && tar -xzf utf8proc-$v.tar.gz\
    && rm -f utf8proc-$v.tar.gz\
    && mv utf8proc-$v utf8proc
}

klib() {
  U=https://raw.github.com/attractivechaos/klib/master
  rm -rf klib
  mkdir -p klib\
    && wget -q $U/khash.h -O klib/khash.h\
    && wget -q $U/kstring.h -O klib/kstring.h\
    && wget -q $U/kstring.c -O klib/kstring.c
}

ylib() {
  U=http://g.blicky.net/ylib.git/plain
  rm -rf ylib
  mkdir -p ylib\
    && wget -q $U/dbusev.c -O ylib/dbusev.c\
    && wget -q $U/dbusev.h -O ylib/dbusev.h\
    && wget -q $U/evtp.c -O ylib/evtp.c\
    && wget -q $U/evtp.h -O ylib/evtp.h\
    && wget -q $U/sqlasync.c -O ylib/sqlasync.c\
    && wget -q $U/sqlasync.h -O ylib/sqlasync.h\
    && wget -q $U/ylog.c -O ylib/ylog.c\
    && wget -q $U/ylog.h -O ylib/ylog.h\
    && wget -q $U/yopt.h -O ylib/yopt.h\
    && wget -q $U/yuri.c -O ylib/yuri.c\
    && wget -q $U/yuri.h -O ylib/yuri.h
}

freetiger() {
  rm -rf freetiger
  git clone git://git.code.sf.net/p/freetiger/code freetiger-code\
    && mv freetiger-code/C freetiger\
    && rm -rf freetiger-code
}

aclean() {
  rm -f lean.m4
  hg clone https://bitbucket.org/GregorR/autoconf-lean
  make -C autoconf-lean\
    && mv autoconf-lean/lean.m4 .\
    && rm -rf autoconf-lean
}


update() {
  klib
  ylib
  freetiger
  aclean
}

all() {
  update
  ev
  utf8proc
}

"$@"
