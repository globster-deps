/**
 * Copyright (c) 2012 Francisco Blas Izquierdo Riera (klondike)
 * The Tiger algorithm was written by Eli Biham and Ross Anderson and is
 * available on the official Tiger algorithm page.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    the algorithm authorsip notice, this list of conditions and the following
 *    disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 4. If this license is not appropriate for you please write me at
 *    klondike ( a t ) klondike ( d o t ) es to negotiate another license.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/

/**
 * These are some implementations of tiger made without looking at the original
 * reference code to ensure the resulting code can be published under a free
 * license. The paper was looked though to know how did tiger work.
 */

//TODO: remove later
#include <stdio.h>

#include <string.h>
#include "tiger.h"
#ifdef __SSE2__
#include <emmintrin.h>
#ifdef __SSE3__
#include <pmmintrin.h>
#endif
#ifdef __SSE4_1__
#include <smmintrin.h>
#endif
#endif
#ifdef _MSC_VER
#include <intrin.h>
#endif

#if defined(__GNUC__) && !defined(NO_USE_PREFETCH)
#define prefetch_rnt(p) __builtin_prefetch((p),0,0);
#elif defined(_MSC_VER) && _M_IX86_FP >= 1 && !defined(NO_USE_PREFETCH)
#include <mmintrin.h>
#define prefetch_rnt(p) _mm_prefetch((char *)(p),_MM_HINT_NTA);
#else
#define prefetch_rnt(p)
#endif

#ifdef __SSE2__
#if defined(_MSC_VER) || defined(__ICC)
#define sse_align(var) __declspec(align(16)) var
#else
#define sse_align(var) var __attribute__((aligned(16)))
#endif
#else
/* Not SSE2 alignment not required */
#define sse_align(var) var
#endif

/* Fortunately the only code that is endian dependent is found on the tiger code
 * space
 */

#include "ttable.h"

// MS VC compiler is braindead
#ifdef _MSC_VER
#define inline __inline
#endif

#define gb(r,a) ((t_word)((uint8_t)(r>>(a*8))))
#define tround_std(mul,a,b,c,x) {\
r##c ^= i##x;\
r##a -= t1[gb(r##c,0)] ^ t2[gb(r##c,2)] ^ t3[gb(r##c,4)] ^ t4[gb(r##c,6)] ;\
r##b += t4[gb(r##c,1)] ^ t3[gb(r##c,3)] ^ t2[gb(r##c,5)] ^ t1[gb(r##c,7)] ;\
r##b *= mul;\
}

#define pass(a,b,c,mul,type) {\
tround_##type(mul,a,b,c,0);\
tround_##type(mul,b,c,a,1);\
tround_##type(mul,c,a,b,2);\
tround_##type(mul,a,b,c,3);\
tround_##type(mul,b,c,a,4);\
tround_##type(mul,c,a,b,5);\
tround_##type(mul,a,b,c,6);\
tround_##type(mul,b,c,a,7);\
}

#define key_sched_std() {\
i0 -= i7 ^ UINT64_C(0xA5A5A5A5A5A5A5A5);\
i1 ^= i0;\
i2 += i1;\
i3 -= i2 ^ ((~(i1))<<19);\
i4 ^= i3;\
i5 += i4;\
i6 -= i5 ^ ((~(i4))>>23);\
i7 ^= i6;\
i0 += i7;\
i1 -= i0 ^ ((~(i7))<<19);\
i2 ^= i1;\
i3 += i2;\
i4 -= i3 ^ ((~(i2))>>23);\
i5 ^= i4;\
i6 += i5;\
i7 -= i6 ^ UINT64_C(0x0123456789ABCDEF);\
}

#define processb(type)\
pass(0,1,2,5,type);\
key_sched_##type();\
pass(2,0,1,7,type);\
key_sched_##type();\
pass(1,2,0,9,type);


static void tiger_block(const t_block in, t_res res) {
    register t_word i0 = *in++;
    register t_word i1 = *in++;
    register t_word i2 = *in++;
    register t_word i3 = *in++;
    register t_word i4 = *in++;
    register t_word i5 = *in++;
    register t_word i6 = *in++;
    register t_word i7 = *in++;
    register t_word r0 = res[0];
    register t_word r1 = res[1];
    register t_word r2 = res[2];
    processb(std);
    res[0] = r0 ^ res[0];
    res[1] = r1 - res[1];
    res[2] = r2 + res[2];
}

static inline void interleave_res6 (t_word res[6]) {
    t_word tmp = res[1];
    res[1]=res[2];
    res[2]=res[4];
    res[4]=res[3];
    res[3]=tmp;
}

static inline void deinterleave_res6 (t_word res[6]) {
    t_word tmp=res[3];
    res[3]=res[4];
    res[4]=res[2];
    res[2]=res[1];
    res[1]=tmp;
}

static void tiger_block_2seq(const t_block in1, const t_block in2, t_word res[6]) {
    interleave_res6(res);
    tiger_block(in1, res);
    tiger_block(in2, res+3);
    deinterleave_res6(res);
}

#define tround_2(mul,a,b,c,x) {\
r1##c ^= i1##x;\
r2##c ^= i2##x;\
r1##a -= t1[gb(r1##c,0)] ^ t2[gb(r1##c,2)] ^ t3[gb(r1##c,4)] ^ t4[gb(r1##c,6)] ;\
r2##a -= t1[gb(r2##c,0)] ^ t2[gb(r2##c,2)] ^ t3[gb(r2##c,4)] ^ t4[gb(r2##c,6)] ;\
r1##b += t4[gb(r1##c,1)] ^ t3[gb(r1##c,3)] ^ t2[gb(r1##c,5)] ^ t1[gb(r1##c,7)] ;\
r2##b += t4[gb(r2##c,1)] ^ t3[gb(r2##c,3)] ^ t2[gb(r2##c,5)] ^ t1[gb(r2##c,7)] ;\
r1##b *= mul;\
r2##b *= mul;\
}

#define key_sched_2() {\
i10 -= i17 ^ UINT64_C(0xA5A5A5A5A5A5A5A5);\
i20 -= i27 ^ UINT64_C(0xA5A5A5A5A5A5A5A5);\
i11 ^= i10;\
i21 ^= i20;\
i12 += i11;\
i22 += i21;\
i13 -= i12 ^ ((~(i11))<<19);\
i23 -= i22 ^ ((~(i21))<<19);\
i14 ^= i13;\
i24 ^= i23;\
i15 += i14;\
i25 += i24;\
i16 -= i15 ^ ((~(i14))>>23);\
i26 -= i25 ^ ((~(i24))>>23);\
i17 ^= i16;\
i27 ^= i26;\
i10 += i17;\
i20 += i27;\
i11 -= i10 ^ ((~(i17))<<19);\
i21 -= i20 ^ ((~(i27))<<19);\
i12 ^= i11;\
i22 ^= i21;\
i13 += i12;\
i23 += i22;\
i14 -= i13 ^ ((~(i12))>>23);\
i24 -= i23 ^ ((~(i22))>>23);\
i15 ^= i14;\
i25 ^= i24;\
i16 += i15;\
i26 += i25;\
i17 -= i16 ^ UINT64_C(0x0123456789ABCDEF);\
i27 -= i26 ^ UINT64_C(0x0123456789ABCDEF);\
}

static void tiger_block_2int(const t_block in1, const t_block in2, t_word res[6]) {
    register t_word i10 = *in1++;
    register t_word i20 = *in2++;
    register t_word i11 = *in1++;
    register t_word i21 = *in2++;
    register t_word i12 = *in1++;
    register t_word i22 = *in2++;
    register t_word i13 = *in1++;
    register t_word i23 = *in2++;
    register t_word i14 = *in1++;
    register t_word i24 = *in2++;
    register t_word i15 = *in1++;
    register t_word i25 = *in2++;
    register t_word i16 = *in1++;
    register t_word i26 = *in2++;
    register t_word i17 = *in1++;
    register t_word i27 = *in2++;
    register t_word r10 = res[0];
    register t_word r20 = res[1];
    register t_word r11 = res[2];
    register t_word r21 = res[3];
    register t_word r12 = res[4];
    register t_word r22 = res[5];
    processb(2);
    res[0] = r10 ^ res[0];
    res[1] = r20 ^ res[1];
    res[2] = r11 - res[2];
    res[3] = r21 - res[3];
    res[4] = r12 + res[4];
    res[5] = r22 + res[5];
}

#ifdef __SSE2__
#if defined(_MSC_VER)
#define ext_fst(a) ((a).m128i_i64[0])
#define ext_snd(a) ((a).m128i_i64[1])
#elif ! defined(__ICC) && defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6))
#define ext_fst(a) ((a)[0])
#define ext_snd(a) ((a)[1])
#elif defined(HASX64) && defined(__SSE4_1__)
#define ext_fst(a) (_mm_extract_epi64((a),0))
#define ext_snd(a) (_mm_extract_epi64((a),1))
#elif defined(HASX64) || defined(__ICC)
#define ext_fst(a) (_mm_cvtsi128_si64((a)))
#define ext_snd(a) (_mm_cvtsi128_si64(_mm_srli_si128((a),8)))
#else
#define ext_fst(a) ((t_word)_mm_movepi64_pi64((a)))
#define ext_snd(a) ((t_word)_mm_movepi64_pi64(_mm_srli_si128((a),8)))
#endif

#define tround_sse2(mul,a,b,c,x) {\
r1##c ^= ext_fst(i##x);\
r2##c ^= ext_snd(i##x);\
r1##a -= t1[gb(r1##c,0)] ^ t2[gb(r1##c,2)] ^ t3[gb(r1##c,4)] ^ t4[gb(r1##c,6)] ;\
r2##a -= t1[gb(r2##c,0)] ^ t2[gb(r2##c,2)] ^ t3[gb(r2##c,4)] ^ t4[gb(r2##c,6)] ;\
r1##b += t4[gb(r1##c,1)] ^ t3[gb(r1##c,3)] ^ t2[gb(r1##c,5)] ^ t1[gb(r1##c,7)] ;\
r2##b += t4[gb(r2##c,1)] ^ t3[gb(r2##c,3)] ^ t2[gb(r2##c,5)] ^ t1[gb(r2##c,7)] ;\
r1##b *= mul;\
r2##b *= mul;\
}


#define do_sse2mul5(r) _mm_add_epi64(_mm_slli_epi64((r),2),r)
#define do_sse2mul7(r) _mm_sub_epi64(_mm_slli_epi64((r),3),r)
#define do_sse2mul9(r) _mm_add_epi64(_mm_slli_epi64((r),3),r)

#ifdef __SSE4_1__
#define gt_td(t,r,p) mm_load(t+_mm_extract_epi8(r,p),t+_mm_extract_epi8(r,8+p))
#define tround_sse2_td(mul,a,b,c,x) {\
r##c = _mm_xor_si128(r##c,i##x);\
r##a = _mm_sub_epi64(r##a,\
  _mm_xor_si128(gt_td(t1,r##c,0),\
     _mm_xor_si128(gt_td(t2,r##c,2),\
        _mm_xor_si128(gt_td(t3,r##c,4),\
           gt_td(t4,r##c,6)))));\
r##b = _mm_add_epi64(r##b,\
  _mm_xor_si128(gt_td(t4,r##c,1),\
     _mm_xor_si128(gt_td(t3,r##c,3),\
        _mm_xor_si128(gt_td(t2,r##c,5),\
           gt_td(t1,r##c,7)))));\
r##b=do_sse2mul##mul(r##b);\
}
#else
#define gt_td(t,r,p) mm_load(t+_mm_extract_epi16(r,p),t+_mm_extract_epi16(r,4+p))
#define tround_sse2_td(mul,a,b,c,x) {\
r##c = _mm_xor_si128(r##c,i##x);\
register __m128i tmp=_mm_and_si128(r##c,mm_set1(c4));\
r##a = _mm_sub_epi64(r##a,\
  _mm_xor_si128(gt_td(t1,tmp,0),\
     _mm_xor_si128(gt_td(t2,tmp,1),\
        _mm_xor_si128(gt_td(t3,tmp,2),\
           gt_td(t4,tmp,3)))));\
tmp=_mm_and_si128(_mm_srli_si128(r##c,1),mm_set1(c4));\
r##b = _mm_add_epi64(r##b,\
  _mm_xor_si128(gt_td(t4,tmp,0),\
     _mm_xor_si128(gt_td(t3,tmp,1),\
        _mm_xor_si128(gt_td(t2,tmp,2),\
           gt_td(t1,tmp,3)))));\
r##b=do_sse2mul##mul(r##b);\
}
#endif

#define mm_load(a,b) _mm_castpd_si128(_mm_loadh_pd(_mm_load_sd((double *)(a)), (double *)(b)))
#ifdef __SSE3__
#define mm_ldf(p) _mm_lddqu_si128((__m128i *)(p))
#else
#define mm_ldf(p) _mm_loadu_si128((__m128i *)(p))
#endif
#define mm_stf(p,r) _mm_storeu_si128((__m128i *)(p),(r))

#ifdef _MSC_VER
#define c11 0xA5A5A5A5
#define c12 0xA5A5A5A5
#define c21 0xFFFFFFFF
#define c22 0xFFFFFFFF
#define c31 0x01234567
#define c32 0x89ABCDEF
#define c41 0x00FF00FF
#define c42 0x00FF00FF
#define mm_set1(a) _mm_set_epi32(a##1,a##2,a##1,a##2)
#else
#define c1 UINT64_C(0xA5A5A5A5A5A5A5A5)
#define c2 UINT64_C(0xFFFFFFFFFFFFFFFF)
#define c3 UINT64_C(0x0123456789ABCDEF)
#define c4 UINT64_C(0x00FF00FF00FF00FF)
#if defined(HASX64) || defined(__ICC)
#define mm_set1(a) _mm_set1_epi64x(a)
#else
#define mm_set1(a) _mm_set1_epi64((__m64)(a))
#endif
#endif

#define key_sched_sse2() {\
i0 = _mm_sub_epi64(i0,_mm_xor_si128(i7,mm_set1(c1)));\
i1 = _mm_xor_si128(i1,i0);\
i2 = _mm_add_epi64(i2,i1);\
i3 = _mm_sub_epi64(i3,_mm_xor_si128(i2,_mm_slli_epi64(_mm_xor_si128(i1,mm_set1(c2)),19)));\
i4 = _mm_xor_si128(i4,i3);\
i5 = _mm_add_epi64(i5,i4);\
i6 = _mm_sub_epi64(i6,_mm_xor_si128(i5,_mm_srli_epi64(_mm_xor_si128(i4,mm_set1(c2)),23)));\
i7 = _mm_xor_si128(i7,i6);\
i0 = _mm_add_epi64(i0,i7);\
i1 = _mm_sub_epi64(i1,_mm_xor_si128(i0,_mm_slli_epi64(_mm_xor_si128(i7,mm_set1(c2)),19)));\
i2 = _mm_xor_si128(i2,i1);\
i3 = _mm_add_epi64(i3,i2);\
i4 = _mm_sub_epi64(i4,_mm_xor_si128(i3,_mm_srli_epi64(_mm_xor_si128(i2,mm_set1(c2)),23)));\
i5 = _mm_xor_si128(i5,i4);\
i6 = _mm_add_epi64(i6,i5);\
i7 = _mm_sub_epi64(i7,_mm_xor_si128(i6,mm_set1(c3)));\
}

#define key_sched_sse2_td key_sched_sse2


static void tiger_block_sse2(const t_block in1, const t_block in2, t_word res[6]) {
    register __m128i i0 = mm_load(in1+0,in2+0);
    register __m128i i1 = mm_load(in1+1,in2+1);
    register __m128i i2 = mm_load(in1+2,in2+2);
    register __m128i i3 = mm_load(in1+3,in2+3);
    register __m128i i4 = mm_load(in1+4,in2+4);
    register __m128i i5 = mm_load(in1+5,in2+5);
    register __m128i i6 = mm_load(in1+6,in2+6);
    register __m128i i7 = mm_load(in1+7,in2+7);
    register t_word r10 = res[0];
    register t_word r20 = res[1];
    register t_word r11 = res[2];
    register t_word r21 = res[3];
    register t_word r12 = res[4];
    register t_word r22 = res[5];
    processb(sse2);
    res[0] = r10 ^ res[0];
    res[1] = r20 ^ res[1];
    res[2] = r11 - res[2];
    res[3] = r21 - res[3];
    res[4] = r12 + res[4];
    res[5] = r22 + res[5];
}

static void tiger_block_sse2_td(const t_block in1, const t_block in2, t_word res[6]) {
    register __m128i i0 = mm_load(in1+0,in2+0);
    register __m128i i1 = mm_load(in1+1,in2+1);
    register __m128i i2 = mm_load(in1+2,in2+2);
    register __m128i i3 = mm_load(in1+3,in2+3);
    register __m128i i4 = mm_load(in1+4,in2+4);
    register __m128i i5 = mm_load(in1+5,in2+5);
    register __m128i i6 = mm_load(in1+6,in2+6);
    register __m128i i7 = mm_load(in1+7,in2+7);
    register __m128i r0 = mm_ldf(res+0);
    register __m128i r1 = mm_ldf(res+2);
    register __m128i r2 = mm_ldf(res+4);
    processb(sse2_td);
    mm_stf(res+0,_mm_xor_si128(r0,mm_ldf(res+0)));
    mm_stf(res+2,_mm_sub_epi64(r1,mm_ldf(res+2)));
    mm_stf(res+4,_mm_add_epi64(r2,mm_ldf(res+4)));
}


#ifdef _MSC_VER
#undef c11
#undef c12
#undef c21
#undef c22
#undef c31
#undef c32
#else
#undef c1
#undef c2
#undef c3
#endif

#endif

#if ! defined(__ICC) && defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6 && ( __GNUC_PATCH >= 4 || defined(__SSE2__))))
#define tround_v2(mul,a,b,c,x) {\
r##c ^= i##x;\
register v2tw xo1 = {t1[gb(r##c[0],0)],t1[gb(r##c[1],0)]};\
register v2tw xo2 = {t2[gb(r##c[0],2)],t2[gb(r##c[1],2)]};\
xo1 ^= xo2;\
register v2tw xo3 = {t3[gb(r##c[0],4)],t3[gb(r##c[1],4)]};\
xo1 ^= xo3;\
register v2tw xo4 = {t4[gb(r##c[0],6)],t4[gb(r##c[1],6)]};\
xo1 ^= xo4;\
r##a -= xo1;\
register v2tw xo5 = {t4[gb(r##c[0],1)],t4[gb(r##c[1],1)]};\
register v2tw xo6 = {t3[gb(r##c[0],3)],t3[gb(r##c[1],3)]};\
xo5 ^= xo6;\
register v2tw xo7 = {t2[gb(r##c[0],5)],t2[gb(r##c[1],5)]};\
xo5 ^= xo7;\
register v2tw xo8 = {t1[gb(r##c[0],7)],t1[gb(r##c[1],7)]};\
xo5 ^= xo8;\
r##b += xo5;\
register v2tw xo9 = {mul,mul};\
r##b *= xo9;\
}

static const v2tw c1_v2 = {UINT64_C(0xA5A5A5A5A5A5A5A5),UINT64_C(0xA5A5A5A5A5A5A5A5)};
static const v2tw c2_v2 = {UINT64_C(0x0123456789ABCDEF),UINT64_C(0x0123456789ABCDEF)};

#define key_sched_v2() {\
i0 -= i7 ^ c1_v2;\
i1 ^= i0;\
i2 += i1;\
i3 -= i2 ^ ((~(i1))<<19);\
i4 ^= i3;\
i5 += i4;\
i6 -= i5 ^ ((~(i4))>>23);\
i7 ^= i6;\
i0 += i7;\
i1 -= i0 ^ ((~(i7))<<19);\
i2 ^= i1;\
i3 += i2;\
i4 -= i3 ^ ((~(i2))>>23);\
i5 ^= i4;\
i6 += i5;\
i7 -= i6 ^ c2_v2;\
}

void tiger_block_v2(const t_block in1, const t_block in2, t_word res[6]) {
    register v2tw i0 = {in1[0],in2[0]};
    register v2tw i1 = {in1[1],in2[1]};
    register v2tw i2 = {in1[2],in2[2]};
    register v2tw i3 = {in1[3],in2[3]};
    register v2tw i4 = {in1[4],in2[4]};
    register v2tw i5 = {in1[5],in2[5]};
    register v2tw i6 = {in1[6],in2[6]};
    register v2tw i7 = {in1[7],in2[7]};
    register v2tw r0 = *(v2tw*)(res+0);
    register v2tw r1 = *(v2tw*)(res+2);
    register v2tw r2 = *(v2tw*)(res+4);
    processb(v2);
    *(v2tw*)(res+0) = r0^(*(v2tw*)(res+0));
    *(v2tw*)(res+2) = r1-(*(v2tw*)(res+2));
    *(v2tw*)(res+4) = r2+(*(v2tw*)(res+4));
}

#endif
static void (*tiger_block_2)(const t_block in1, const t_block in2, t_word res[6]) =
#if defined(__SSE4_1__)
tiger_block_sse2_td;
#elif defined(__SSE2__)
tiger_block_sse2;
#else
tiger_block_2seq;
#endif
void (*tiger_2) (const char *str1, const char *str2, t_word length, t_res res1, t_res res2) = tiger_2int;
void (*tiger_2_49)(const t_word h[12], t_word res[6]) = tiger_2int_49;
void (*tiger_2_1025)(const char str[2048], t_word res[6]) = tiger_2int_1025;

// #if ! defined(__ICC) && defined (__GNUC__)
// 
// typedef uint64_t v4res_ __attribute__ ((vector_size (32)));
// typedef union v4res {
//    v4res_ v;
//    uint64_t d[4];
// } v4res;
// 
// static const v4res_ v4cd19 = {1<<19,1<<19,1<<19,1<<19};
// static const v4res_ v4cd23 = {1<<23,1<<23,1<<23,1<<23};
// static const v4res_ v4cks1 = {UINT64_C(0xA5A5A5A5A5A5A5A5),UINT64_C(0xA5A5A5A5A5A5A5A5),UINT64_C(0xA5A5A5A5A5A5A5A5),UINT64_C(0xA5A5A5A5A5A5A5A5)};
// static const v4res_ v4cks2 = {UINT64_C(0x0123456789ABCDEF),UINT64_C(0x0123456789ABCDEF),UINT64_C(0x0123456789ABCDEF),UINT64_C(0x0123456789ABCDEF)};
// 
// #define gb(r,a) ((r>>(a*8))&0xff)
// #define tround_v4(mul,a,b,c,x) {\
// r1##c ^= (t_word)i##x.d[0];\
// r2##c ^= (t_word)i##x.d[1];\
// r3##c ^= (t_word)i##x.d[2];\
// r4##c ^= (t_word)i##x.d[3];\
// r1##a -= t1[gb(r1##c,0)] ^ t2[gb(r1##c,2)] ^ t3[gb(r1##c,4)] ^ t4[gb(r1##c,6)] ;\
// r2##a -= t1[gb(r2##c,0)] ^ t2[gb(r2##c,2)] ^ t3[gb(r2##c,4)] ^ t4[gb(r2##c,6)] ;\
// r3##a -= t1[gb(r3##c,0)] ^ t2[gb(r3##c,2)] ^ t3[gb(r3##c,4)] ^ t4[gb(r3##c,6)] ;\
// r4##a -= t1[gb(r4##c,0)] ^ t2[gb(r4##c,2)] ^ t3[gb(r4##c,4)] ^ t4[gb(r4##c,6)] ;\
// r1##b += t4[gb(r1##c,1)] ^ t3[gb(r1##c,3)] ^ t2[gb(r1##c,5)] ^ t1[gb(r1##c,7)] ;\
// r2##b += t4[gb(r2##c,1)] ^ t3[gb(r2##c,3)] ^ t2[gb(r2##c,5)] ^ t1[gb(r2##c,7)] ;\
// r3##b += t4[gb(r3##c,1)] ^ t3[gb(r3##c,3)] ^ t2[gb(r3##c,5)] ^ t1[gb(r3##c,7)] ;\
// r4##b += t4[gb(r4##c,1)] ^ t3[gb(r4##c,3)] ^ t2[gb(r4##c,5)] ^ t1[gb(r4##c,7)] ;\
// r1##b *= mul;\
// r2##b *= mul;\
// r3##b *= mul;\
// r4##b *= mul;\
// }
// 
// #define pass_v4(a,b,c,mul) {\
// tround_v4(mul,a,b,c,0);\
// tround_v4(mul,b,c,a,1);\
// tround_v4(mul,c,a,b,2);\
// tround_v4(mul,a,b,c,3);\
// tround_v4(mul,b,c,a,4);\
// tround_v4(mul,c,a,b,5);\
// tround_v4(mul,a,b,c,6);\
// tround_v4(mul,b,c,a,7);\
// }
// 
// 
// 
// #define key_sched_v4() {\
// i0.v -= i7.v ^ v4cks1;\
// i1.v ^= i0.v;\
// i2.v += i1.v;\
// i3.v -= i2.v ^ ((~(i1.v))*v4cd19);\
// i4.v ^= i3.v;\
// i5.v += i4.v;\
// i6.v -= i5.v ^ ((~(i4.v))/v4cd23);\
// i7.v ^= i6.v;\
// i0.v += i7.v;\
// i1.v -= i0.v ^ ((~(i7.v))*v4cd19);\
// i2.v ^= i1.v;\
// i3.v += i2.v;\
// i4.v -= i3.v ^ ((~(i2.v))/v4cd23);\
// i5.v ^= i4.v;\
// i6.v += i5.v;\
// i7.v -= i6.v ^ v4cks2;\
// }
// 
// static void tiger_block_v4(const v4res in[8], t_res res1, t_res res2, t_res res3, t_res res4) {
//     register v4res i0 = in[0];
//     register v4res i1 = in[1];
//     register v4res i2 = in[2];
//     register v4res i3 = in[3];
//     register v4res i4 = in[4];
//     register v4res i5 = in[5];
//     register v4res i6 = in[6];
//     register v4res i7 = in[7];
//     register t_word r10 = res1[0];
//     register t_word r11 = res1[1];
//     register t_word r12 = res1[2];
//     register t_word r20 = res2[0];
//     register t_word r21 = res2[1];
//     register t_word r22 = res2[2];
//     register t_word r30 = res3[0];
//     register t_word r31 = res3[1];
//     register t_word r32 = res3[2];
//     register t_word r40 = res4[0];
//     register t_word r41 = res4[1];
//     register t_word r42 = res4[2];
//     pass_v4(0,1,2,5);
//     key_sched_v4();
//     pass_v4(2,0,1,7);
//     key_sched_v4();
//     pass_v4(1,2,0,9);
//     res1[0] = r10 ^ res1[0];
//     res1[1] = r11 - res1[1];
//     res1[2] = r12 + res1[2];
//     res2[0] = r20 ^ res2[0];
//     res2[1] = r21 - res2[1];
//     res2[2] = r22 + res2[2];
//     res3[0] = r30 ^ res3[0];
//     res3[1] = r31 - res3[1];
//     res3[2] = r32 + res3[2];
//     res4[0] = r40 ^ res4[0];
//     res4[1] = r41 - res4[1];
//     res4[2] = r42 + res4[2];
// }
// #endif

#define uc(x) ((unsigned char *)(x))
#ifdef USE_BIG_ENDIAN\

#define endianvars \
int j,k;\
unsigned char swp;

#define endianvars_2 \
int j,k;\
unsigned char swp1, swp2;


#else
#define endianvars
#define endianvars_2
#endif


void tiger(const char *str, t_word length, t_res res)
{
    t_block tmp;
    const char * end = str + (length&(-64));
    t_word i;
    endianvars
    res[0]=UINT64_C(0x0123456789ABCDEF);
    res[1]=UINT64_C(0xFEDCBA9876543210);
    res[2]=UINT64_C(0xF096A5B4C3B2E187);
    
    while(str<end)
    {
        #ifdef USE_BIG_ENDIAN
        for (j = 0; j < 8; j++) {
            for (k = 0; k < 8; k++) {
                uc(tmp+j)[7-k] = *str++;
            }
        }
        tiger_block(tmp, res);
        #else
        #ifdef FORCE_ALIGNMENT
        memcpy(tmp,str,64);
        str+=64;
        tiger_block(tmp, res);
        #else
        tiger_block((const t_word *)str, res);
        str+=64;
        #endif
        #endif
    }
    i=length & 63;
    //Padding on last block
    //add 0x01 afterwards.
    //Then set to 0 the rest of the bytes of the word
    //If we have a whole block now encrypt and start with empty block
    //Finally add message size at the end of the block.
    memcpy(tmp,str,(size_t)i);
    uc(tmp)[i++]=0x01;
    memset(uc(tmp)+i,0,(size_t)((8-i)&7));
    i+=(8-i)&7;
    //Reorder the block so it uses propper endianism now
    //TODO: recode this so it copies the data and sets it in big endian 
    #ifdef USE_BIG_ENDIAN
    for (j = 0; j < i; j+=8) {
        for (k = 0; k < 4; k++) {
            swp = uc(tmp)[j+k];
            uc(tmp)[j+k] = uc(tmp)[j+(7-k)];
            uc(tmp)[j+(7-k)] = swp;
        }
    }
    #endif
    if(i==64) {
        tiger_block(tmp, res);
        i=0;
    }
    //Reordering here is not needed anymore since these ops are done with propper endian
    memset(uc(tmp)+i,0,(size_t)(56-i));
    (tmp[7])=length<<(t_word)3;
    tiger_block(tmp, res);
}

void tiger_2int(const char *str1, const char *str2, t_word length, t_res res1, t_res res2)
{
    t_block tmp1;
    t_block tmp2;
    const char * end = str1 + (length&(-64));
    t_word i;
    endianvars_2
    sse_align(t_word res[6])={UINT64_C(0x0123456789ABCDEF),UINT64_C(0x0123456789ABCDEF),
                   UINT64_C(0xFEDCBA9876543210),UINT64_C(0xFEDCBA9876543210),
                   UINT64_C(0xF096A5B4C3B2E187),UINT64_C(0xF096A5B4C3B2E187)};
    
    while(str1<end)
    {
        #ifdef USE_BIG_ENDIAN
        for (j = 0; j < 8; j++) {
            for (k = 0; k < 8; k++) {
                uc(tmp1+j)[7-k] = *str1++;
                uc(tmp2+j)[7-k] = *str2++;
            }
        }
        tiger_block_2(tmp1, tmp2, res);
        #else
        #ifdef FORCE_ALIGNMENT
        memcpy(tmp1,str1,64);
        memcpy(tmp2,str2,64);
        str1+=64;
        str2+=64;
        tiger_block_2(tmp1, tmp2, res);
        #else
        tiger_block_2((const t_word *)str1, (const t_word *)str2, res);
        str1+=64;
        str2+=64;
        #endif
        #endif
    }
    i=length & 63;
    //Padding on last block
    //add 0x01 afterwards.
    //Then set to 0 the rest of the bytes of the word
    //If we have a whole block now encrypt and start with empty block
    //Finally add message size at the end of the block.
    memcpy(tmp1,str1,(size_t)i);
    memcpy(tmp2,str2,(size_t)i);
    uc(tmp1)[i]=0x01;
    uc(tmp2)[i++]=0x01;
    memset(uc(tmp1)+i,0,(size_t)((8-i)&7));
    memset(uc(tmp2)+i,0,(size_t)((8-i)&7));
    i+=(8-i)&7;
    //TODO put the blocks here with propper endianism by default
    //Reorder the block so it uses propper endianism now
    #ifdef USE_BIG_ENDIAN
    for (j = 0; j < i; j+=8) {
        for (k = 0; k < 4; k++) {
            swp1 = uc(tmp1)[j+k];
            swp2 = uc(tmp2)[j+k];
            uc(tmp1)[j+k] = uc(tmp1)[j+(7-k)];
            uc(tmp2)[j+k] = uc(tmp2)[j+(7-k)];
            uc(tmp1)[j+(7-k)] = swp1;
            uc(tmp2)[j+(7-k)] = swp2;
        }
    }
    #endif
    if(i==64) {
        tiger_block_2(tmp1, tmp2, res);
        i=0;
    }
    memset(uc(tmp1)+i,0,(size_t)(56-i));
    memset(uc(tmp2)+i,0,(size_t)(56-i));
    tmp1[7]=length<<(t_word)3;
    tmp2[7]=length<<(t_word)3;
    tiger_block_2(tmp1, tmp2, res);
    res1[0]=res[0];
    res2[0]=res[1];
    res1[1]=res[2];
    res2[1]=res[3];
    res1[2]=res[4];
    res2[2]=res[5];
}

// #if ! defined(__ICC) && defined (__GNUC__)
// void tiger_v4(const char *str1, const char *str2, const char *str3, const char *str4, t_word length, t_res res1, t_res res2,t_res res3, t_res res4)
// {
//     v4res in[8];
//     
//     const char * end = str1 + (length&(-64));
//     t_word i;
//     int c1,c2;
//     
//     res1[0]=UINT64_C(0x0123456789ABCDEF);
//     res2[0]=UINT64_C(0x0123456789ABCDEF);
//     res3[0]=UINT64_C(0x0123456789ABCDEF);
//     res4[0]=UINT64_C(0x0123456789ABCDEF);
//     res1[1]=UINT64_C(0xFEDCBA9876543210);
//     res2[1]=UINT64_C(0xFEDCBA9876543210);
//     res3[1]=UINT64_C(0xFEDCBA9876543210);
//     res4[1]=UINT64_C(0xFEDCBA9876543210);
//     res1[2]=UINT64_C(0xF096A5B4C3B2E187);
//     res2[2]=UINT64_C(0xF096A5B4C3B2E187);
//     res3[2]=UINT64_C(0xF096A5B4C3B2E187);
//     res4[2]=UINT64_C(0xF096A5B4C3B2E187);
// 
//     while(str1<end)
//     {
//         for (i = 0; i < 8; i++) {
//             #ifdef USE_BIG_ENDIAN
//                 for (k = 0; k < 8; k++) {
//                     uc(in[i].d[0])[7-k] = *str1++;
//                     uc(in[i].d[1])[7-k] = *str2++;
//                     uc(in[i].d[2])[7-k] = *str3++;
//                     uc(in[i].d[3])[7-k] = *str4++;
//                 }
//             #else
//                 memcpy(in[i].d+0,str1,8);
//                 str1+=8;
//                 memcpy(in[i].d+1,str2,8);
//                 str2+=8;
//                 memcpy(in[i].d+2,str3,8);
//                 str3+=8;
//                 memcpy(in[i].d+3,str4,8);
//                 str4+=8;
//             #endif
//         }
//         tiger_block_v4(in,res1,res2,res3,res4);
//     }
//     //Padding on last block
//     //add 0x01 afterwards.
//     //Then set to 0 the rest of the bytes of the word
//     //If we have a whole block now encrypt and start with empty block
//     //Finally add message size at the end of the block.
//     for (i = 0; i < (length & 63); i++) {
//         c1 = i >> 3; c2 = i & 7;
//         #ifdef USE_BIG_ENDIAN
//             uc(in[c1].d+0)[7-(c2)] = *str1++;
//             uc(in[c1].d+1)[7-(c2)] = *str2++;
//             uc(in[c1].d+2)[7-(c2)] = *str3++;
//             uc(in[c1].d+3)[7-(c2)] = *str4++;
//         #else
//             uc(in[c1].d+0)[c2] = *str1++;
//             uc(in[c1].d+1)[c2] = *str2++;
//             uc(in[c1].d+2)[c2] = *str3++;
//             uc(in[c1].d+3)[c2] = *str4++;
//         #endif
//     }
//     c1 = i >> 3; c2 = i & 7;
//     #ifdef USE_BIG_ENDIAN
//         uc(in[c1].d+0)[7-(c2)] = 0x01;
//         uc(in[c1].d+1)[7-(c2)] = 0x01;
//         uc(in[c1].d+2)[7-(c2)] = 0x01;
//         uc(in[c1].d+3)[7-(c2)] = 0x01;
//     #else
//         uc(in[c1].d+0)[c2] = 0x01;
//         uc(in[c1].d+1)[c2] = 0x01;
//         uc(in[c1].d+2)[c2] = 0x01;
//         uc(in[c1].d+3)[c2] = 0x01;
//     #endif
//     i++;c2++;
//     while(i&7) {
//         #ifdef USE_BIG_ENDIAN
//             uc(in[c1].d+0)[7-(c2)] = 0x00;
//             uc(in[c1].d+1)[7-(c2)] = 0x00;
//             uc(in[c1].d+2)[7-(c2)] = 0x00;
//             uc(in[c1].d+3)[7-(c2)] = 0x00;
//         #else
//             uc(in[c1].d+0)[c2] = 0x00;
//             uc(in[c1].d+1)[c2] = 0x00;
//             uc(in[c1].d+2)[c2] = 0x00;
//             uc(in[c1].d+3)[c2] = 0x00;
//         #endif
//         i++;c2++;
//     }
//     if(i==64) {
//         tiger_block_v4(in,res1,res2,res3,res4);
//         i=0;
//     }
//     //Reordering here is not needed anymore since these ops are done with propper endian
//     for(c1 = i >>3; c1 < 7; c1++) {
//         in[c1].d[0] = 0;
//         in[c1].d[1] = 0;
//         in[c1].d[2] = 0;
//         in[c1].d[3] = 0;
//     }
//     in[7].d[0]=length<<(t_word)3;
//     in[7].d[1]=length<<(t_word)3;
//     in[7].d[2]=length<<(t_word)3;
//     in[7].d[3]=length<<(t_word)3;
//     tiger_block_v4(in,res1,res2,res3,res4);
// }
// #endif

/**
 * These are optimized versions for some fixed sizes frequent in TTHs: 1025 and
 * 49
 * They will automatically add the 0x00 or 0x01 byte required by the tree so
 * the data buffer you use MUST be at least 1024 or 48 bytes long**/
/** Ensure t_res are in local endian or this will give incorrect results**/
/** the two hashes here must be 64 bit aligned! **/
void tiger_49(const t_res h1, const t_res h2, t_res res)
{
    t_block tmp;
    int i;
    unsigned char ec1=0x01;
    unsigned char ec2=(h1[2])>>56;
    
    res[0]=UINT64_C(0x0123456789ABCDEF);
    res[1]=UINT64_C(0xFEDCBA9876543210);
    res[2]=UINT64_C(0xF096A5B4C3B2E187);
    
    for (i=0; i < 3; i++) {
        tmp[i] = (t_word)(h1[i] << (t_word)8) | (t_word)ec1;
        tmp[3+i] = (t_word)(h2[i] << (t_word)8) | (t_word)ec2;
        ec1 = h1[i] >> (t_word)56;
        ec2 = h2[i] >> (t_word)56;
    }
    tmp[6]=(t_word)ec2|(t_word)0x0100;
    tmp[7]=(t_word)49<<(t_word)3;
    tiger_block(tmp, res);
}

void tiger_1025(const char *str, t_res res)
{
    t_block tmp;
    const char *end=str+1023;
    endianvars
    
    res[0]=UINT64_C(0x0123456789ABCDEF);
    res[1]=UINT64_C(0xFEDCBA9876543210);
    res[2]=UINT64_C(0xF096A5B4C3B2E187);
    
    #ifdef USE_BIG_ENDIAN
    uc(tmp)[7]=0x00;
    for (k = 0; k < 7; k++) {
        uc(tmp)[(6-k)] = *str++;
    }
    for (j = 1; j < 8; j++) {
        for (k = 0; k < 8; k++) {
            uc(tmp+j)[7-k] = *str++;
        }
    }
    #else
    *uc(tmp)=0x00;
    memcpy(uc(tmp)+1,str,63);
    str+=63;
    #endif
    tiger_block(tmp, res);
    do {
        #ifdef USE_BIG_ENDIAN
        for (j = 0; j < 8; j++) {
            for (k = 0; k < 8; k++) {
                uc(tmp+j)[7-k] = *str++;
            }
        }
        tiger_block(tmp, res);
        #else
        #ifdef FORCE_ALIGNMENT
        memcpy(tmp,str,64);
        str+=64;
        tiger_block(tmp, res);
        #else
        tiger_block((const t_word *)str, res);
        str+=64;
        #endif
        #endif
    } while (str != end);
    *tmp=(*(unsigned char *)str)|(0x0100);
    #if defined(IS_LITTLE_ENDIAN) && defined(USE_BIG_ENDIAN)
    //HACK: we swap the bytes since the previous calculation is done in big endian on tiger but on little endian here.
    for (k = 0; k < 4; k++) {
        swp = uc(tmp)[k];
        uc(tmp)[k] = uc(tmp)[(7-k)];
        uc(tmp)[(7-k)] = swp;
    }
    #endif
    memset(tmp+1,0,48);
    tmp[7]=(t_word)1025<<(t_word)3;
    tiger_block(tmp, res);
}

//TODO: make faster
void tiger_data(const char *str, uint64_t size, t_res res)
{
    char c[1025];
    c[0]=0x0;
    memcpy(c+1,str,size);
    tiger(c,size+1,res);
}

/* These set of functions add the nodetype mark at the end instead of the beggining and are there for benchmarking purpouses.
 * Don't use them Since they'll just produce invalid results
 */

void tigerf_49(const t_res h1, const t_res h2, t_res res)
{
    t_block tmp;
    
    /* result from hashing a block of 0s*/
    res[0]=UINT64_C(0x47098b7964e8a968);
    res[1]=UINT64_C(0x41b26306875ba3a2);
    res[2]=UINT64_C(0x157c6f58e1e5fc30);
    
    memcpy(tmp,h1,sizeof(t_res));
    memcpy(tmp+3,h2,sizeof(t_res));
    tmp[6]=0x01;
    tmp[7]=(t_word)112<<(t_word)3; /* 64 + 48 */
    tiger_block(tmp, res);
}

/*Final block to hash*/
static const t_block finalf_1025 = {
    0x01,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    (t_word)1088<<(t_word)3 /* 1024+64*/
};

void tigerf_1025(const char *str, t_res res)
{
    int i;
    
    /* result from hashing a block of 0s with a 1 byte in the begining*/
    res[0]=UINT64_C(0xad89e0488bb78d75);
    res[1]=UINT64_C(0x23944705c3538381);
    res[2]=UINT64_C(0xb86ed726e0fffc35);
    
    /* TODO: big endian implementation */
    for (i = 0; i < 1024; i+=64) {
        tiger_block((t_word *)(str+i), res);
    }
    tiger_block(finalf_1025, res);
}

//TODO: make faster
void tigerf_data(const char *str, uint64_t size, t_res res)
{
    char c[1088];
    memset(c,0,64);
    c[0]=0x1;
    memcpy(c+64,str,size);
    tiger(c,size+64,res);
}

/**
 * Like the above but with 2 blocks instead of one, it is faster.
 */
void tiger_2int_49(const t_word h[12], t_word res[6])
{
    t_block tmp1;
    t_block tmp2;
    int i;
    unsigned char ec11=0x01;
    unsigned char ec12=0x01;
    unsigned char ec21=((h+(0*3))[2])>>56;
    unsigned char ec22=((h+(2*3))[2])>>56;
    t_word tmp;
    prefetch_rnt(h);
    
    res[0]=res[1]=UINT64_C(0x0123456789ABCDEF);
    res[2]=res[3]=UINT64_C(0xFEDCBA9876543210);
    res[4]=res[5]=UINT64_C(0xF096A5B4C3B2E187);
    
    for (i=0; i < 3; i++) {
        tmp1[i] = (t_word)((h+(0*3))[i] << (t_word)8) | (t_word)ec11;
        tmp1[3+i] = (t_word)((h+(1*3))[i] << (t_word)8) | (t_word)ec21;
        tmp2[i] = (t_word)((h+(2*3))[i] << (t_word)8) | (t_word)ec12;
        tmp2[3+i] = (t_word)((h+(3*3))[i] << (t_word)8) | (t_word)ec22;
        ec11 = (h+(0*3))[i] >> (t_word)56;
        ec21 = (h+(1*3))[i] >> (t_word)56;
        ec12 = (h+(2*3))[i] >> (t_word)56;
        ec22 = (h+(3*3))[i] >> (t_word)56;
    }
    tmp1[6]=(t_word)ec21|(t_word)0x0100;
    tmp2[6]=(t_word)ec22|(t_word)0x0100;
    tmp1[7]=(t_word)49<<(t_word)3;
    tmp2[7]=(t_word)49<<(t_word)3;
    tiger_block_2(tmp1, tmp2, res);
    
    interleave_res6(res);
}

void tiger_2int_1025(const char str[2048], t_word res[6])
{
    t_block tmp1;
    t_block tmp2;
    const char *end=str+1023;
    endianvars_2
    prefetch_rnt(str);
    prefetch_rnt(str+1024);
    res[0]=res[1]=UINT64_C(0x0123456789ABCDEF);
    res[2]=res[3]=UINT64_C(0xFEDCBA9876543210);
    res[4]=res[5]=UINT64_C(0xF096A5B4C3B2E187);
    
    #ifdef USE_BIG_ENDIAN
    uc(tmp1)[7]=0x00;
    uc(tmp2)[7]=0x00;
    for (k = 0; k < 7; k++) {
        uc(tmp1)[(6-k)] = *str;
        uc(tmp2)[(6-k)] = *(str+1024);
        str++;
    }
    prefetch_rnt(str+64);
    for (j = 1; j < 8; j++) {
        for (k = 0; k < 8; k++) {
            uc(tmp1+j)[7-k] = *str;
            uc(tmp2+j)[7-k] = *(str+1024);
            str++;
        }
    }
    prefetch_rnt(str+64+1024);
    #else
    *uc(tmp1)=0x00;
    *uc(tmp2)=0x00;
    memcpy(uc(tmp1)+1,str,63);
    prefetch_rnt(str+64);
    memcpy(uc(tmp2)+1,str+1024,63);
    prefetch_rnt(str+64+1024);
    str+=63;
    #endif
    tiger_block_2(tmp1, tmp2, res);
    do {
        #ifdef USE_BIG_ENDIAN
        for (j = 0; j < 8; j++) {
            for (k = 0; k < 8; k++) {
                uc(tmp1+j)[7-k] = *str;
                uc(tmp2+j)[7-k] = *(str+1024);
                str++;
            }
        }
        prefetch_rnt(str);
        prefetch_rnt(str+1024);
        tiger_block_2(tmp1, tmp2, res);
        #else
        #ifdef FORCE_ALIGNMENT
        memcpy(tmp1,str,64);
        prefetch_rnt(str+64);
        memcpy(tmp2,str+1024,64);
        prefetch_rnt(str+64+1024);
        str+=64;
        tiger_block_2(tmp1, tmp2, res);
        #else
        prefetch_rnt(str+64);
        prefetch_rnt(str+64+1024);
        tiger_block_2((const t_word *)str, (const t_word *)(str+1024), res);
        str+=64;
        #endif
        #endif
    } while (str != end);
    *tmp1=(*(unsigned char *)str)|(0x0100);
    *tmp2=(*(unsigned char *)(str+1024))|(0x0100);
    #if defined(IS_LITTLE_ENDIAN) && defined(USE_BIG_ENDIAN)
    //HACK: we swap the bytes since the previous calculation is done in big endian on tiger but on little endian here.
    for (k = 0; k < 4; k++) {
        swp1 = uc(tmp1)[k];
        swp2 = uc(tmp2)[k];
        uc(tmp1)[k] = uc(tmp1)[(7-k)];
        uc(tmp2)[k] = uc(tmp2)[(7-k)];
        uc(tmp1)[(7-k)] = swp1;
        uc(tmp2)[(7-k)] = swp2;
    }
    #endif
    memset(tmp1+1,0,48);
    memset(tmp2+1,0,48);
    tmp1[7]=(t_word)1025<<(t_word)3;
    tmp2[7]=(t_word)1025<<(t_word)3;
    tiger_block_2(tmp1, tmp2, res);
    interleave_res6(res);
}

// #if ! defined(__ICC) && defined (__GNUC__)
// void tiger_v4_1025(const char *str1, const char *str2, const char *str3, const char *str4, t_res res1, t_res res2,t_res res3, t_res res4)
// {
//     v4res in[8];
//     
//     const char * end = str1 + 1023;
//     t_word i;
//     int c1,c2;
//     
//     res1[0]=UINT64_C(0x0123456789ABCDEF);
//     res2[0]=UINT64_C(0x0123456789ABCDEF);
//     res3[0]=UINT64_C(0x0123456789ABCDEF);
//     res4[0]=UINT64_C(0x0123456789ABCDEF);
//     res1[1]=UINT64_C(0xFEDCBA9876543210);
//     res2[1]=UINT64_C(0xFEDCBA9876543210);
//     res3[1]=UINT64_C(0xFEDCBA9876543210);
//     res4[1]=UINT64_C(0xFEDCBA9876543210);
//     res1[2]=UINT64_C(0xF096A5B4C3B2E187);
//     res2[2]=UINT64_C(0xF096A5B4C3B2E187);
//     res3[2]=UINT64_C(0xF096A5B4C3B2E187);
//     res4[2]=UINT64_C(0xF096A5B4C3B2E187);
// 
//     #ifdef USE_BIG_ENDIAN
//         uc(in[c1].d+0)[7] = 0;
//         uc(in[c1].d+1)[7] = 0;
//         uc(in[c1].d+2)[7] = 0;
//         uc(in[c1].d+3)[7] = 0;
//     #else
//         uc(in[c1].d+0)[0] = 0;
//         uc(in[c1].d+1)[0] = 0;
//         uc(in[c1].d+2)[0] = 0;
//         uc(in[c1].d+3)[0] = 0;
//     #endif
//     i= 1;
//     do {
//         c1 = i >> 3; c2 = i & 7;
//         #ifdef USE_BIG_ENDIAN
//             uc(in[c1].d+0)[7-(c2)] = *str1++;
//             uc(in[c1].d+1)[7-(c2)] = *str2++;
//             uc(in[c1].d+2)[7-(c2)] = *str3++;
//             uc(in[c1].d+3)[7-(c2)] = *str4++;
//         #else
//             uc(in[c1].d+0)[c2] = *str1++;
//             uc(in[c1].d+1)[c2] = *str2++;
//             uc(in[c1].d+2)[c2] = *str3++;
//             uc(in[c1].d+3)[c2] = *str4++;
//         #endif
//         i++;
//         if ( i == 64) {
//             tiger_block_v4(in,res1,res2,res3,res4);
//             i=0;
//         }
//     } while(str1<end);
// 
//     in[0].d[0]=(*(unsigned char *)str1)|(0x0100);
//     in[0].d[1]=(*(unsigned char *)str2)|(0x0100);
//     in[0].d[2]=(*(unsigned char *)str3)|(0x0100);
//     in[0].d[3]=(*(unsigned char *)str4)|(0x0100);
//     for (i = 1; i < 7; i++) {
//         in[i].d[0]=0;
//         in[i].d[1]=0;
//         in[i].d[2]=0;
//         in[i].d[3]=0;
//     }
//     in[7].d[0]=1025<<(t_word)3;
//     in[7].d[1]=1025<<(t_word)3;
//     in[7].d[2]=1025<<(t_word)3;
//     in[7].d[3]=1025<<(t_word)3;
//     tiger_block_v4(in,res1,res2,res3,res4);
// }
// #endif

void tiger_2seq(const char *str1, const char *str2, t_word length, t_res res1, t_res res2) {
    tiger(str1,length,res1);
    tiger(str2,length,res2);
}

void tiger_2seq_49(const t_word h[12], t_word res[6]) {
    tiger_49(h+0,h+3,res+0);
    tiger_49(h+6,h+9,res+3);
}

void tiger_2seq_1025(const char str[2048], t_word res[6]) {
    tiger_1025(str+0,res+0);
    tiger_1025(str+1024,res+3);
}

void tigerp1(const char *password, t_word length, const char *salt, t_pres *pres)
{
    t_block tmp;
    const char * end = password + (length&(-64));
    t_word i;
    endianvars
    t_word *res = pres->h;
    res[0]=UINT64_C(0x0123456789ABCDEF);
    res[1]=UINT64_C(0xFEDCBA9876543210);
    res[2]=UINT64_C(0xF096A5B4C3B2E187);
    
    //Initialize the presult salt
    memcpy(pres->r,salt,128);
    pres->n = 0;
    pres->hs = length;
    
    while(password<end)
    {
        #ifdef USE_BIG_ENDIAN
        for (j = 0; j < 8; j++) {
            for (k = 0; k < 8; k++) {
                uc(tmp+j)[7-k] = *password++;
            }
        }
        tiger_block(tmp, res);
        #else
        #ifdef FORCE_ALIGNMENT
        memcpy(tmp,password,64);
        password+=64;
        tiger_block(tmp, res);
        #else
        tiger_block((const t_word *)password, res);
        password+=64;
        #endif
        #endif
    }
    //Now concatenate some SALT bits
    i=length & 63;
    memcpy(tmp,password,(size_t)i);
    pres->n = 64-((size_t)i);
    memcpy(uc(tmp)+i,salt,pres->n);
    #ifdef USE_BIG_ENDIAN
    for (j = 0; j < 8; j++) {
        for (k = 0; k < 4; k++) {
            swp = uc(tmp+j)[k];
            uc(tmp+j)[k] = uc(tmp+j)[7-k];
            uc(tmp+j)[7-k] = swp;
        }
    }
    #endif
    tiger_block(tmp, res);
    //Process the extra 64 SALT bits
    salt+=pres->n;
    pres->n+=64;
    #ifdef USE_BIG_ENDIAN
    for (j = 0; j < 8; j++) {
        for (k = 0; k < 8; k++) {
            uc(tmp+j)[7-k] = *salt++;
        }
    }
    tiger_block(tmp, res);
    #else
    #ifdef FORCE_ALIGNMENT
    memcpy(tmp,salt,64);
    salt+=64;
    tiger_block(tmp, res);
    #else
    tiger_block((const t_word *)salt, res);
    salt+=64;
    #endif
    #endif
    pres->hs += pres->n;
    //Finally we reorder the result so it is shown in little endian
}


void tigerp2(const t_pres *pres, const char *salt, t_word length, t_res res)
{
    t_block tmp;
    const char * end = salt + length;
    t_word i;
    endianvars
    
    memcpy(res, pres->h, 24);
    
    //Append any remaining psalt character
    i=128 - pres->n;
    memcpy(tmp,pres->r+pres->n,i);
    while (i != 64 && salt != end) {
        uc(tmp)[i++] = *salt++;
    }
    
    if ( i == 64) {
        #ifdef USE_BIG_ENDIAN
        for (j = 0; j < 8; j++) {
            for (k = 0; k < 4; k++) {
                swp = uc(tmp+j)[k];
                uc(tmp+j)[k] = uc(tmp+j)[7-k];
                uc(tmp+j)[7-k] = swp;
            }
        }
        #endif
        tiger_block(tmp, res);
        i = length+64-pres->n;
        end = salt + (i&(-64));
        while(salt<end)
        {
            #ifdef USE_BIG_ENDIAN
            for (j = 0; j < 8; j++) {
                for (k = 0; k < 8; k++) {
                    uc(tmp+j)[7-k] = *salt++;
                }
            }
            tiger_block(tmp, res);
            #else
            #ifdef FORCE_ALIGNMENT
            memcpy(tmp,salt,64);
            salt+=64;
            tiger_block(tmp, res);
            #else
            tiger_block((const t_word *)salt, res);
            salt+=64;
            #endif
            #endif
        }
        //Now concatenate the last SALT bits
        i = i &63;
        memcpy(tmp,salt,i);
    }
    //Padding on last block
    //add 0x01 afterwards.
    //Then set to 0 the rest of the bytes of the word
    //If we have a whole block now encrypt and start with empty block
    //Finally add message size at the end of the block.
    uc(tmp)[i++]=0x01;
    memset(uc(tmp)+i,0,(size_t)((8-i)&7));
    i+=(8-i)&7;
    //Reorder the block so it uses propper endianism now
    //TODO: recode this so it copies the data and sets it in big endian 
    #ifdef USE_BIG_ENDIAN
    for (j = 0; j < i; j+=8) {
        for (k = 0; k < 4; k++) {
            swp = uc(tmp)[j+k];
            uc(tmp)[j+k] = uc(tmp)[j+(7-k)];
            uc(tmp)[j+(7-k)] = swp;
        }
    }
    #endif
    if(i==64) {
        tiger_block(tmp, res);
        i=0;
    }
    //Reordering here is not needed anymore since these ops are done with propper endian
    memset(uc(tmp)+i,0,(size_t)(56-i));
    (tmp[7])=(pres->hs+length+(128 - pres->n))<<(t_word)3;
    tiger_block(tmp, res);
}

#ifdef USE_BIG_ENDIAN
/**Swaps the result endianism to little endian for hexadecimal displaying**/
void t_to_from_littleendian(unsigned char* r) {
    int i, j;
    unsigned char c;
    for(i = 0; i < 3; i++) {
        for (j = 0; j < 4; j++) {
            c = r[j];
            r[j]=r[8-j];
            r[8-j]=c;
        }
        c+=8;
    }
}

/**Process 2 hashes in paralel for higher speeds**/
void t_to_from_littleendian2(t_res r1, t_res r2) {
    t_to_from_littleendian(r1);
    t_to_from_littleendian(r2);
}
    
/**Similar to the above but with an array of hashes**/
void t_to_from_littleendianv(t_res *r, int nres) {
    int i;
    for (i = 0; i < nres; i++)
        t_to_from_littleendian(r[i]);
}

/**This is the version for the partial hash result type for machine interoperability when using binary data**/
void tp_to_from_littleendianv(t_pres *pres) {
    int k;
    unsigned char swp;
    t_to_from_littleendian(pres.h);
    for (j = 0; j < 4; j++) {
        swp = uc(pres.n)[j];
        uc(pres.n)[j]=uc(pres.n)[8-j];
        uc(pres.n)[8-j]=swp;
    }
    for (j = 0; j < 4; j++) {
        swp = uc(pres.hs)[j];
        uc(pres.hs)[j]=uc(pres.hs)[8-j];
        uc(pres.hs)[8-j]=swp;
    }
}
#endif

static inline uint64_t flog2(uint64_t a) {
    #if defined(__GNUC__) && __SIZEOF_LONG_LONG__ >= 8
        return 63-__builtin_clzll(a);
    #elif defined(_MSC_VER) && defined(_M_AMD64)
        unsigned long r;
        _BitScanReverse64(&r,a);
        return r;
    #elif defined(_MSC_VER) && defined(_M_IX86)
        unsigned long r;
        if(_BitScanReverse(&r,a>>UINT64_C(32)))
        return r+32;
        _BitScanReverse(&r,(unsigned long)a);
        return r;
    #else
        register uint64_t r;
        register uint64_t shift;
            r = (a > UINT64_C(0xFFFFFFFF)) << 5; a >>= r;
        shift = (a > UINT64_C(0xFFFF    )) << 4; a >>= shift; r |= shift;
        shift = (a > UINT64_C(0xFF      )) << 3; a >>= shift; r |= shift;
        shift = (a > UINT64_C(0xF       )) << 2; a >>= shift; r |= shift;
        shift = (a > UINT64_C(0x3       )) << 1; a >>= shift; r |= shift;
        r |= (a >> 1);
        return r;
    #endif
}

static inline uint64_t clog2(uint64_t a) {
    return 1 + flog2(a-1);
}

static inline uint64_t nblocks(uint64_t v) {
    return (v+1023)/1024;
}

static inline uint64_t left(uint64_t v) {
    return (1 << flog2((v-1)&-1024));
}

void do_tth (const char *a, uint64_t size, t_res data) {
    int level = 0;
    int maxlevel= clog2(size+1023)-11;//log_2 1024 * 2 (since we return 2 hashes)
    t_word * s= (t_word *)malloc(sizeof(t_res)*(maxlevel+1)*4);
    int *pos = (int *)calloc(maxlevel,sizeof(int));
    if (size < 1024) {
      tiger_data(a,size,data);
      return;
    }
    do {
        if (level == 0) {
            //TODO: issue madvise
            if (size >= 1024*4) {
                tiger_2_1025(a+(1024*0),s+((level)*4+0)*3);
                tiger_2_1025(a+(1024*2),s+((level)*4+2)*3);
                a+=1024*4;
                size-=1024*4;
                pos[0]=0;
            } else if (size > 1024*3) {
                tiger_2_1025(a+(1024*0),s+((level)*4+0)*3);
                tiger_1025(a+(1024*2),s+((level)*4+2)*3);
                tiger_data(a+(1024*3),size-1024*3,s+((level)*4+3)*3);
                size = 0;
                pos[0]=0;
            } else if (size == 1024*3) {
                tiger_2_1025(a+(1024*0),s+((level)*4+0)*3);
                tiger_1025(a+(1024*2),s+((level)*4+2)*3);
                size = 0;
                pos[0]=3;
            } else if (size > 1024*2) {
                tiger_2_1025(a+(1024*0),s+((level)*4+0)*3);
                tiger_data(a+(1024*2),size-1024*2,s+((level)*4+2)*3);
                size = 0;
                pos[0]=3;
            } else if (size == 1024*2) {
                tiger_2_1025(a+(1024*0),s+((level)*4+0)*3);
                size = 0;
                pos[0]=2;
            } else if (size > 1024) {
                tiger_1025(a+(1024*0),s+((level)*4+0)*3);
                tiger_data(a+(1024*1),size-1024,s+((level)*4+1)*3);
                size = 0;
                pos[0]=2;
            } else if (size == 1024) {
                tiger_1025(a+(1024*0),s+((level)*4+0)*3);
                size = 0;
                pos[0]=1;
            } else {
                tiger_data(a+(1024*0),size,s+((level)*4+0)*3);
                size = 0;
                pos[0]=1;
            }
            level++;
        } else {
            if (size == 0) {
                switch (pos[level-1]) {
                    case 0:
                        tiger_2_49(s+((level-1)*4+0)*3,s+((level)*4+pos[level])*3);
                        pos[level]=(pos[level]+2)&3;
                        break;
                    case 3:
                        tiger_49(s+((level-1)*4+0)*3,s+((level-1)*4+1)*3,s+((level)*4+pos[level])*3);
                        memcpy(s+((level)*4+pos[level]+1)*3,s+((level-1)*4+2)*3,sizeof(t_res)); //TODO improve: Pass the hash upwards
                        pos[level]=(pos[level]+2)&3;
                        break;
                    case 2:
                        tiger_49(s+((level-1)*4+0)*3,s+((level-1)*4+1)*3,s+((level)*4+pos[level])*3);
                        pos[level]=(pos[level]+1)&3;
                        break;
                    case 1:
                        memcpy(s+((level)*4+pos[level])*3,s+((level-1)*4+0)*3,sizeof(t_res)); //TODO improve: Pass the hash upwards
                        pos[level]=(pos[level]+1)&3;
                        break;
                }
                level++;
            } else {
                tiger_2_49(s+((level-1)*4+0)*3,s+((level)*4+pos[level])*3);
                pos[level]=(pos[level]+2)&3;
                if (pos[level] == 0)
                    level++;
                else
                    level = 0;
            }
        }
    } while(level < maxlevel);
    switch (pos[level-1]) {
        case 0:
            tiger_2_49(s+((level-1)*4+0)*3,s+((level)*4)*3);
            tiger_49(s+((level)*4+0)*3,s+((level)*4+1)*3,data);
            break;
        case 3:
            tiger_49(s+((level-1)*4+0)*3,s+((level-1)*4+1)*3,s+((level)*4)*3);
            tiger_49(s+((level)*4+0)*3,s+((level-1)*4+2)*3,data);
            break;
        case 2:
            tiger_49(s+((level-1)*4+0)*3,s+((level-1)*4+1)*3,data);
            break;
        case 1:
            memcpy(data,s+((level-1)*4+0)*3,sizeof(t_res)); //TODO improve: Pass the hash upwards
            break;
    }
    free(s);
    free(pos);
}

uint64_t tth_leaves(uint64_t size, int level) {
    uint64_t aleft = left(size);
    uint64_t accum = 0;
    while (1) {
        if (level <= 1 || size == 0)
            return accum+1;
        if ( nblocks(size) <= (uint64_t)1 << ((uint64_t)level-1))
            return accum+nblocks(size);
        aleft = left(size);
        accum += ((aleft / 1024 <= ((uint64_t)level-1))?aleft/1024:((uint64_t)1)<<((uint64_t)level-2));
        size-=aleft;
        level--;
    }
}

//TODO: merge_trees
#ifndef MINBLOCKSIZE
#define MINBLOCKSIZE 64*1024
#endif

static inline uint64_t get_block_size(uint64_t size, int level) {
    uint64_t tmp = 2048 << (clog2(nblocks(size)) - level) ;
    return (MINBLOCKSIZE> tmp) ? MINBLOCKSIZE : tmp;
}

static inline uint64_t get_block_number(uint64_t size, uint64_t block_size) {
    return size < 1023? 1:(size + (block_size - 1))/ block_size;
}

static inline t_tth * alloc_t_tth (uint64_t size, int level) {
    uint64_t bs = get_block_size(size, level);
    uint64_t bn = get_block_number(size,bs);
    t_tth *d = (t_tth *)malloc (sizeof(t_tth)+sizeof(t_word)*3*bn);
    d->nblocks = bn;
    d->block_size = bs;
    d->level = level;
    return d;
}

t_tth * tth (const char *a, uint64_t size, int level) {
    t_tth *r = alloc_t_tth(size,level);
    int64_t i;
    #pragma omp parallel shared(r,a,size)
    {
        #pragma omp for schedule(dynamic,1)
        for ( i = 0; i < (int64_t)r->nblocks; i++) {
            do_tth(a+i*r->block_size, ((i == r->nblocks - 1)? size - i *r->block_size :r->block_size), r->data+(3*i));
        }
    }
    return r;
}

void merge_tth (const t_tth * th, t_res r) {
    int level = 0;
    int maxlevel= th->level-1;
    t_word * s= (t_word *)malloc(sizeof(t_res)*(maxlevel+1)*4);
    int *pos = (int *)calloc(maxlevel,sizeof(int));
    uint64_t nb = th->nblocks;
    t_word *a=(t_word *)(th->data);
    do {
        if (nb >= 4) {
            if (level == 0) {
                tiger_2_49(a,s+((level)*4+pos[level])*3);
                a+=3*4;
                nb-=4;
            } else {
                tiger_2_49(s+((level-1)*4+0)*3,s+((level)*4+pos[level])*3);
            }
            pos[level]=(pos[level]+2)&3;
            if (pos[level] == 0 || nb == 0)
                level++;
            else
                level = 0;
        } else {
            switch (nb) {
                default: // If 0 we are done with the blocks just get out
                    switch (pos[level-1]) {
                        case 0:
                            tiger_2_49(s+((level-1)*4+0)*3,s+((level)*4+pos[level])*3);
                            pos[level]+=2;
                            break;
                        case 3:
                            tiger_49(s+((level-1)*4+0)*3,s+((level-1)*4+1)*3,s+((level)*4+pos[level])*3);
                            memcpy(s+((level)*4+pos[level]+1)*3,s+((level-1)*4+2)*3,sizeof(t_res)); //TODO improve: Pass the hash upwards
                            pos[level]+=2;
                            break;
                        case 2:
                            tiger_49(s+((level-1)*4+0)*3,s+((level-1)*4+1)*3,s+((level)*4+pos[level])*3);
                            pos[level]+=1;
                            break;
                        case 1:
                            memcpy(s+((level)*4+pos[level])*3,s+((level-1)*4+0)*3,sizeof(t_res)); //TODO improve: Pass the hash upwards
                            pos[level]+=1;
                            break;
                    }
                    break;
                case 3:
                    tiger_49(a,a+3,s+((level)*4+pos[level])*3);
                    memcpy(s+((level)*4+pos[level]+1)*3,a+6,sizeof(t_res)); //TODO improve: Pass the hash upwards
                    nb = 0;
                    pos[level]+=2;
                    break;
                case 2:
                    tiger_49(a,a+3,s+((level)*4+pos[level])*3);
                    nb = 0;
                    pos[level]+=1;
                    break;
                case 1:
                    memcpy(s+((level)*4+pos[level])*3,a,sizeof(t_res)); //TODO improve: Pass the hash upwards
                    nb = 0;
                    pos[level]+=1;
                    break;
            }
            pos[level]&=3;
            level++;
        }
    } while(level < maxlevel);
    switch (pos[level-1]) {
        case 2:
            tiger_49(s+((level-1)*4+0)*3,s+((level-1)*4+1)*3,r);
            break;
        case 1:
            memcpy(r,s+((level-1)*4+0)*3,sizeof(t_res)); //TODO improve: Pass the hash upwards
            break;
        default:
            puts("UHOH");
            break;
    }
    free(s);
    free(pos);
}            
      


struct jobdata {
    uint64_t offset;
    uint64_t size;
};

//Level starts at 0
// void tth_level (uint64_t level, void * aux, uint64_t size, t_word *data) {
//     struct jobdata *jds = (struct jobdata *)malloc((1<<level)*sizeof(struct jobdata));
//     int i,j;
//     //Step 1: generate the jobs (done sequentially)
//     if(1024<<level > size) {
//         //Iteratively fill the jobs
//         for (i = 0; i * 1024 < size; i++) {
//             jds[i].size = 1024;
//             jds[i].offset = i*1024;
//         }
//     } else {
//         //Recursively fill the jobs
//         //TODO: make iterative
//         i=fill_recur(0,size,jds,level);
//     }
//     //Get TTHs
//     #pragma omp parallel shared(i,jds,aux); 
//     {
//     #pragma omp for schedule(dynamic, 1)
//     for (j = 0; j < i; j++) {
//         tth(aux,data,jds[i].offset, jds[i].size);
//     }
//     }
//         
//         
//     free(jds);
// }
// 
// void fill_recur (uint64_t offset, uint64_t size, struct jobdata jds, uint64_t level) {
//     if (level == 0) {
//         jds->size = size;
//         jds->offset = offset;
//         return 1;
//     }
//     uint64_t  aleft = left(size);
//     return fill_recur(offset,aleft,jds,level-1) + fill_recur(offset+aleft,size-aleft,jds+(1<<(level-1)),level-1);
// }

// void tth_ (const char * a, uint64_t size, t_res data) {
//     uint64_t lsize;
//     static char fst=1;
//     if(fst) {
//         fst = 0;
//         for (lsize = 1; lsize <= 1024 *200000; lsize += 1024) {
//             printf("%"PRIu64" %"PRIu64"\n", nblocks(lsize), tth_leaves(lsize,7));   
//         }
//         printf("%"PRIu64" %"PRIu64"\n", nblocks(size), tth_leaves(size,7));   
//     }
//     tth_(a,size,data);
// }

// int tth_level (char * a, uint64_t size, int level, t_word *data) {
//     uint64_t nelems = nblocks(size);
//     if()
//     
//     int rlevel = 
// }
